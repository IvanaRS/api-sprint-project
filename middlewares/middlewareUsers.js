const { listaPedidos, orderStatus } = require("../database/orders");
const { usuarios } = require("../database/user");

function validarDatosUsuario(req, res, next) {

    let { username, email, password, telefono, domicilio } = req.body;

    if ( !username || !email || !password || !telefono || !domicilio  ) {

        res.status(404).json({error: `Datos Incompletos !` });

    } else {
        next();
    }
}

function isAdmin(req,res,next) {
    for (let user of usuarios) {

        if (user.id===Number(req.headers.id)) {

            if (user.isAdmin) {
                return next();
                
            }
            else { 
                return res.status(404).json ('No es administrador');
            }
        }
        
    }
    res.status(404).json ('El id no coincide con ningun usuario');
    
}

function isLogged(req,res,next) {
    for (let user of usuarios) {
        if (user.id===Number(req.headers.id)) {

            if (user.logged) {
                return next();
                
            }
            else{
                res.status(404).json('Usted debe iniciar sesión');
            }
            
        }
        
    }
    res.status(404).json ('El id no coincide con ningun usuario');
    
}

function mailDuplicado(req,res,next) {
    for (let user of usuarios) {
        if(user.email===req.body.email){

            return res.status (404).json('El email ingresado ya se encuentra en uso');
        }
    }
    next();
}

function pedidoAbierto(req,res,next) {
  
    for (const pedido  of listaPedidos) {
        if (pedido.idPedido===Number(req.params.idPedido)) {
            if (pedido.orderStatus===1) {
                next();
            } 
            else{

                return res.status(404).json('No puede modificar su pedido');
            }

        }
    }
}

module.exports = { validarDatosUsuario,
                   isAdmin, 
                   isLogged,
                   pedidoAbierto,
                   mailDuplicado};