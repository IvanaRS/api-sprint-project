API - 1er Sprint Project Acamica
Delilah Resto 

Herramientas utilizadas: Visual Studio Code , NodeJS, Express, Swagger.

   Procedimiento :

1 - Instalación 
Clonar proyecto desde la consola :

git clone https://gitlab.com/IvanaRS/api-sprint-project.git

2 -  
 npm install
 npm i express -- save
 npm i express swagger-ui-express
 npm iexpress swagger-ui-express js-yaml

3 - Iniciar el servidor 
En Visual Studio Code ejecutar en terminal :

npm run start

