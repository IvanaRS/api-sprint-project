const { productos } = require("./products");
const { usuarios } = require("./user");

let listaPedidos = [];

class Pedido {
   constructor(id, idPedido, orderStatus, hora, address, idProducto, nombreProducto, montoTotal, medioPago, descripcionPedido) {

      this.id = id;
      this.idPedido = idPedido;
      this.orderStatus = orderStatus;
      this.hora = new Date();
      this.address = address;
      this.idProducto = idProducto;
      this.nombreProducto = nombreProducto;
      this.montoTotal =0;
      this.medioPago = medioPago;
      this.descripcionPedido = descripcionPedido;
   }
}

let orderStatus = [

   {
      "id": 1,
      "estado": "Pendiente",
      "abierto": true
   },
   {
      "id": 2,
      "estado": "Confirmado",
      "abierto": false
   },
   {
      "id": 3,
      "estado": "En preparacion",
      "abierto": false

   },
   {
      "id": 4,
      "estado": "Enviado",
      "abierto": false
   },
   {
      "id": 5,
      "estado": "Entregado",
      "abierto": false
   }
];

let pedidosEntregados = []

function crearPedido(req, res) {
   let newOrder = new Pedido();
   if (listaPedidos.length === 0) {
      newOrder.idPedido = 1;
   } else {
      newOrder.idPedido = listaPedidos[listaPedidos.length - 1].idPedido + 1;
   }
   newOrder.id = Number(req.headers.id);
   newOrder.orderStatus = 1;
   newOrder.address = req.body.address;
   newOrder.descripcionPedido = [];
   
   for (let user of usuarios) {
      if (user.id === Number(req.headers.id)) {
         listaPedidos.push(newOrder);
         return res.status(200).json('Pedido creado con éxito.');
      }

   }
   return res.status(404).json('Complete  todos los campos.');
}

function agregarProducto(req, res) {
   for (let user of usuarios) {
      if (user.id === Number(req.headers.id)) {
         for (let pedido of listaPedidos) {
            if (pedido.idPedido === Number(req.params.idPedido)) {
               for (let producto of productos) {
                  if (producto.idProducto === Number(req.params.idProducto)) {
                     pedido.descripcionPedido.push(producto);
                     return res.status(200).json('Producto agregado a su pedido con exito');
                  }
               }
            }

         }
         return res.status(404).json('Ha ocurrido un error');
      }

   }
}

function verPedidos(req, res) {
   res.status(200).json(listaPedidos);
}

function verPedido(req, res) {
   for (const pedido of listaPedidos) {
      if (pedido.id === Number(req.params.idPedido)) {
         res.status(200).json(pedido);
      }
   }
}

function cambiarStatus(req, res) {
   for (let pedido of listaPedidos) {
      if (pedido.idPedido === Number(req.params.idPedido)) {
         pedido.orderStatus = req.body.orderStatus;
         if (pedido.orderStatus === 5) {
            pedidosEntregados.push(pedido);
            let pedidoIndex = listaPedidos.indexOf(pedido);
            listaPedidos.splice(pedidoIndex, 1);
         }
         return res.status(200).json('Status modificado correctamente');
      }
   }
}

function quitarProducto(req, res) {
   for (let user of usuarios) {
      if (user.id === Number(req.headers.id)) {
         for (let pedido of listaPedidos) {
            if (pedido.idPedido === Number(req.params.idPedido)) {
               for (let producto of productos) {
                  if (producto.idProducto === Number(req.params.idProducto)) {
                     let productoIndex = productos.indexOf(producto);
                     pedido.descripcionPedido.splice(productoIndex, 1);
                     res.status(200).json('Se ha eliminado correctamente');
                  }
               }
            }

         }
         return res.status(404).json('Ha ocurrido un error');
      }

   }
}

function pagarPedido(req, res) {
   for (let user of usuarios) {
      if (user.id === Number(req.headers.id)) {
         for (let pedido of listaPedidos) {
            if ( pedido.idPedido === Number(req.params.idPedido)) {
               pedido.name = req.body.name;
               for (let producto of pedido.descripcionPedido) {
                  pedido.montoTotal += (producto.precio);
               }
            }
            pedido.orderStatus = 2;
            res.status(200).json('Su pedido ha sido pagado correctamente');
         }
      }
   }
   return res.status(404).json('Su pedido no ha sido pagado ');
}

function verHistorial(req, res) {
   for (let user of usuarios) {
      if (user.id === Number(req.headers.id)) {
         for (let Pedido of pedidosEntregados) {
            if (Pedido.idPedido === Number(req.params.idPedido)) {
               if (pedido.id === user.id) {
                  user.historialPedidos.push(pedido);
               }
            }
         }

         return res.status(200).json(user.historialPedidos);
      }
   }
}

module.exports = {
   crearPedido,
   agregarProducto,
   Pedido,
   listaPedidos,
   pagarPedido,
   orderStatus,
   verPedidos,
   verPedido,
   verHistorial,
   pedidosEntregados,
   quitarProducto,
   pagarPedido,
   cambiarStatus
};


