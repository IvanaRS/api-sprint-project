const medioPago = [
    {
        "idMedioPago": 1,
        "name": "efectivo"
    },
    {
        "idMedioPago": 2,
        "name": "tarjeta de credito"
    },
    {
        "idMedioPago": 3,
        "name": "tarjeta de debito"
    }

]

class MetodoPago {
    constructor(idMedioPago, name) {

        this.idMedioPago = idMedioPago;
        this.name = name;
    }
}

function crearMetodo(req, res) {
    let nuevoMetodo = new MetodoPago();
    if (medioPago.length === 0) {
        nuevoMetodo.idMedioPago = 1;
    } else {
        nuevoMetodo.idMedioPago = medioPago[medioPago.length - 1].idMedioPago + 1;
    };
    nuevoMetodo.name = req.body.name;

    medioPago.push(nuevoMetodo);
    return res.status(200).json('El nuevo método de pago ha sido creado con éxito');
}

function verMetodos(req, res) {
    res.json(medioPago);
}

function modificarMetodo(req, res) {
    for (let metodo of medioPago) {
        if (metodo.idMedioPago === Number(req.params.idMedioPago)) {
            metodo.name = req.body.name;
        }
        return res.status(200).json('El método de pago ha sido modificado');
    }
    return res.status(404).json('No se ha podido modificar el método de pago');
}

function eliminarMetodo(req, res) {
    for (let metodo of medioPago) {
        if (metodo.idMedioPago === Number(req.params.idMedioPago)) {
            let metodoIndex = medioPago.indexOf(metodo);
            medioPago.splice(metodoIndex, 1);
            return res.status(200).json('Se ha eliminado correctamente el método de pago');
        }
    }
    return res.status(404).json('No se ha podido eliminar el método de pago');
}

module.exports = {
    medioPago,
    MetodoPago,
    crearMetodo,
    verMetodos,
    modificarMetodo,
    eliminarMetodo

};