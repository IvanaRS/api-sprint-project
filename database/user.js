let usuarios = [
    {
        "id": 1,
        "username": "ivana",
        "email": "ivana.speziale@gmail.com",
        "telephone": "00542901419395",
        "address": "Magallanes 1600",
        "password": "qwerty123",
        "isAdmin": true

    }
    ,
    {
        "id": 2,
        "username": "pablito",
        "email": "pablitoClavoUnClavito@gmail.com",
        "telephone": "00542901419397",
        "address": "Calle Falsa 123",
        "password": "CalleFalsa123",
        "isAdmin": false,
        "historialPedidos": []
    }
];

class User {
    constructor(id, username, email, telephone, address, password, isAdmin, historialPedidos, logged) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.telephone = telephone;
        this.address = address;
        this.password = password;
        this.isAdmin = isAdmin;
        this.historialPedidos = historialPedidos;
        this.logged = logged;

    }
}

function createNewUser(req, res) {
    let newUser = new User();
    newUser.id = req.body.id;
    newUser.username = req.body.username;
    newUser.email = req.body.email;
    newUser.telephone = req.body.telephone;
    newUser.address = req.body.address;
    newUser.password = req.body.password;
    newUser.isAdmin = false;
    newUser.historialPedidos = [];
    newUser.logged = false;

    usuarios.push(newUser);
    res.status(202).send('Usuario creado con éxito');
}

function login(req, res) {
    for (let user of usuarios) {
        if (user.username === req.body.username && user.password === req.body.password) {
            user.logged = true;
            return res.status(200).json('Loggeado!');
        }
    }
    res.status(404).json('Usuario no encontrado');
    return false;
}

function listarUsuarios(req, res) {
    return res.json(usuarios);

}


module.exports = {
    usuarios,
    User,
    createNewUser,
    login,
    listarUsuarios

};

