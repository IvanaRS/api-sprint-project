
let productos= [
    {
        "idProducto":1,
        "nombreProducto":"Bagel de Salmon",
        "precio":400
    },
    {
        "idProducto":2,
        "name":"Hamburguesa clasica",
        "precio":500
    },
    {
        "idProducto":3,
        "name":"Sandwich veggie",
        "precio":300
    },

    {
        "idProducto":4,
        "name":"Ensalada veggie",
        "precio":350
    },
    {
        "idProducto":5,
        "name":"Focaccia",
        "precio":450
    },
    {
        "idProducto":6,
        "name":"Sandwich Focaccia",
        "precio":550
    }
]

class Producto {
    constructor(idProducto,nombreProducto, precio) {
 
       this.idProducto = idProducto;
       this.nombreProducto = nombreProducto;
       this.precio = precio;
    } 
}

function crearProducto(req,res) {
    let nuevoProducto= new Producto();
    if (productos.length === 0) {
        nuevoProducto.idProducto = 1;
     } else {
        nuevoProducto.idProducto = productos[productos.length - 1].idProducto + 1;
     };
    nuevoProducto.nombreProducto=req.body.nombreProducto;
    nuevoProducto.precio=req.body.precio;

    productos.push(nuevoProducto);
    return res.status(200).json('Elnuevo producto ha sido creado con exito');
}

function modificarProducto(req,res) {
    for (let producto of productos) {
        if (producto.id===Number(req.body.idProducto)) {
            producto.nombreProducto=req.body.nombreProducto;
            producto.precio=req.body.precio;
        }
        return res.status(200).json('Se ha modificado correctamente');
    }
    return res.status(404).json('No se ha podido modificar el producto');
}

function eliminarProducto(req,res) {
    for (let producto of productos) {
        if (producto.idProducto===Number(req.params.idProducto)) {
            let productoIndex= productos.indexOf(producto);
            productos.splice(productoIndex, 1);
            return res.status(200).json('Se ha eliminado correctamente');
        }
    }
    return res.status(404).json('No se ha podido eliminar el producto');
}

function verProductos(req,res) {
    return res.json(productos);
}

module.exports={
    productos,
    crearProducto,
    modificarProducto,
    eliminarProducto,
    verProductos
};