const express = require('express');
const {loadSwaggerInfo }=require('./middlewares/documentation.js');
const {login, createNewUser, listarUsuarios}=require('./database/user.js');
const { isAdmin, isLogged, mailDuplicado, pedidoAbierto } = require('./middlewares/middlewareUsers.js');
const { crearPedido, agregarProducto, verPedidos, cambiarStatus, verHistorial, quitarProducto, pagarPedido } = require('./database/orders.js');
const { crearProducto, modificarProducto, eliminarProducto, verProductos } = require('./database/products.js');
const { crearMetodo, verMetodos, modificarMetodo, eliminarMetodo } = require('./database/paymentMethods.js');
const server = express();
server.use(express.json());

function main() {
    loadSwaggerInfo();
    const server = express();
    server.use(express.json());

    server.post('/crearUsuario' ,mailDuplicado,createNewUser);
    server.get('/usuarios',isAdmin, listarUsuarios);
    server.post('/login' ,login);
    
    server.use('/',isLogged);
    server.post('/crearPedido', crearPedido);
    server.get('/pedido',isAdmin,verPedidos);
    server.post('/pedido/:idPedido/:idProducto', pedidoAbierto,agregarProducto);
    server.delete('/pedido/:idPedido/:idProducto',pedidoAbierto, quitarProducto);
    server.put('/pedido/:idPedido',isAdmin,cambiarStatus);
    server.post('/pedido/:idPedido',pagarPedido);
    server.get('/pedido',verHistorial);
    
    server.post('/producto',isAdmin, crearProducto);
    server.put('/producto', isAdmin,modificarProducto);
    server.delete('/producto/:idProducto',isAdmin,eliminarProducto);
    server.get('/producto',isAdmin,verProductos);

    server.post('/metodos',isAdmin,crearMetodo);
    server.get('/metodos',isAdmin,verMetodos);
    server.put('/metodos/:idMedioPago',isAdmin, modificarMetodo);
    server.delete('/metodos/:idMedioPago',isAdmin,eliminarMetodo);
    
    
    server.listen(9093, () => {console.log(`Example app listening on port 9093!`)});
}

main();

